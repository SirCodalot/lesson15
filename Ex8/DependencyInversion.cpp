#include <string>

/**
 * 1. The Email should have a constructor that initializes the private variables.
 *    There should also be an abstract class that holds the to, from, subject and content variables
 * 3. No, I had to write code that was similar to the email class
 */

class Message
{
public:
	Message() = default;
	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void subject(const std::string& subject) {}
	void content(const std::string& content) {}
	virtual void send() const = 0;
};

class Email : public Message
{
public:
	Email() = default;

	void send() const override
	{
		// Send Email
	}
};

class SMS : public Message
{
public:
	SMS() = default;

	void send() const override
	{
		// Send SMS
	}
};

class Reminder
{
	Message *_message;
public:
	Reminder(const std::string& to,
		const std::string& from,
		const std::string& subject,
		const std::string& content) : _message(nullptr)
	{
		_message = new Email();
		_message->to(to);
	}
	~Reminder()
	{
		delete _message;
	}
	void sendReminder() const
	{
		_message->send();
	}
};