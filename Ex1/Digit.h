#pragma once

#include <string>

class Digit
{
	int _value;

public:
	Digit(int value);

	int getValue() const;
	void setValue(int value);

};