#include <iostream>
#include <string>

#include "Digit.h"
#include "InvalidDigitException.h"

bool isNum(std::string text);
bool isNumSimplified(std::string text);

int main()
{
	std::cout << "Normal:" << std::endl;
	std::cout << "  0123 -> " << isNum("0123") << std::endl;
	std::cout << "  123e -> " << isNum("123e") << std::endl;
	std::cout << "  123 -> " << isNum("123") << std::endl;

	std::cout << "Simplified:" << std::endl;
	std::cout << "  0123 -> " << isNumSimplified("0123") << std::endl;
	std::cout << "  123e -> " << isNumSimplified("123e") << std::endl;
	std::cout << "  123 -> " << isNumSimplified("123") << std::endl;

	system("pause");
	return 0;
}

bool isNum(std::string text)
{
	try
	{
		for (int i = 0; i < text.size(); i++)
		{
			Digit digit(text[i] - '0');
			if (!i && !digit.getValue())
			{
				return false;
			}
		}
	}
	catch(InvalidDigitException e)
	{
		return false;
	}

	return true;
}

bool isNumSimplified(std::string text)
{
	for (int i = 0; i < text.size(); i++)
	{
		if (text[i] < '0' || text[i] > '9' || !(text[0] - '0'))
		{
			return false;
		}
	}
	return true;
}
