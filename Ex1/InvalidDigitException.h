#pragma once

#include <exception>

class InvalidDigitException : public std::exception
{
public:
	InvalidDigitException() = default;

	char const* what() const override
	{
		return "A digit's value must be a number between 0 to 9";
	}
};