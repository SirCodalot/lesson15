#include "Digit.h"
#include "InvalidDigitException.h"

Digit::Digit(int value)
{
	setValue(value);
}

int Digit::getValue() const
{
	return _value;
}

void Digit::setValue(int value)
{
	if (value < 0 || value > 9)
	{
		throw InvalidDigitException();
	}

	_value = value;
}
