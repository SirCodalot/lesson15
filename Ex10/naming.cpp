#include <vector>

/**
 * 1. It calculates the average of the numbers in a certain vector
 */

float calcAverage(const std::vector<int>& numbers)
{
	if (numbers.empty())
		throw std::exception("invalid");
	float sum = 0.0;
	for (std::vector<int>::const_iterator iterator = numbers.begin(), e = numbers.end(); iterator != e; ++iterator)
		sum += *iterator;
	return sum / numbers.size();
}