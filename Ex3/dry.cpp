#include <iostream>
#include <cmath>

struct Point
{
	int x;
	int y;
};
double calcTrianglePerimeter(const Point& A, const Point& B, const Point& C)
{
	return std::sqrt((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y)) +
		std::sqrt((C.x - B.x) * (C.x - B.x) + (C.y - B.y) * (A.y - B.y)) +
		std::sqrt((A.x - C.x) * (A.x - C.x) + (A.y - C.y) * (A.y - C.y));
}

double getDistance(const Point& A, const Point& B)
{
	return std::sqrt(std::pow(B.x - A.x, 2) + std::pow(B.y - A.y, 2));
}

double calcTrianglePerimeterDRY(const Point& A, const Point& B, const Point& C)
{
	return getDistance(A, B) + getDistance(B, C) + getDistance(C, A);
}

double calcPentagonPerimeter(const Point& A, const Point& B, const Point& C, const Point& D, const Point& E)
{
	return getDistance(A, B) + getDistance(B, C) + getDistance(C, D) + getDistance(D, E) + getDistance(E, A);
}

double calcShapePerimeter(const Point* points, int amount)
{
	double perimeter = 0;

	for (int i = 0; i < amount; i++)
	{
		perimeter += getDistance(points[i], points[i + 1 == amount ? 0 : i + 1]);
	}

	return perimeter;
}

int main()
{
	std::cout << "1. Given Triangle Function (Fixed): "
		<< calcTrianglePerimeter(Point{ 0, 3 }, Point{ 4, 0 }, Point{ 0, 0 }) << std::endl;
	std::cout << "2. DRY Triangle Function: "
		<< calcTrianglePerimeterDRY(Point{ 0, 3 }, Point{ 4, 0 }, Point{ 0, 0 }) << std::endl;
	std::cout << "3. DRY Pentagon Function: "
		<< calcPentagonPerimeter(Point{ -1, 2 }, Point{ 1, 2 }, Point{ 2, 0 }, Point { 0, -1 }, Point { -2, 0 }) << std::endl;
	
	Point points[5] = { Point{ -1, 2 }, Point{ 1, 2 }, Point{ 2, 0 }, Point{ 0, -1 }, Point{ -2, 0 } };
	std::cout << "4. A Function That Supports DRY: "
		<< calcShapePerimeter(points, 5) << std::endl;

	system("pause");
	return 0;
}