#include <iostream>

class Email
{
	std::string _email;
	
	static bool validateEmail(const std::string& email)
	{
		if (email.find('@') == std::string::npos)
			return false;
		return true;
	}

public:
	Email(const std::string& email)
	{
		if (!validateEmail(email))
			throw std::invalid_argument("email address not valid");
		_email = email;
	}
};

class Person
{
	std::string _firstName;
	std::string _lastName;
	Email _email;

public:
	Person(const std::string& firstName, const std::string& lastName, const std::string& email)
		: _firstName(firstName), _lastName(lastName), _email(Email(email)) {}
};