#include <string>
#include <algorithm>

#define MIN_NAME_LENGTH 2
#define MAX_NAME_LENGTH 20


/**
 * 1. Checks if the name is longer than the minimum
 *    Checks if the name is shorter than the maximum
 *    Checks if the first character is a letter
 *    Checks if the rest of the name is a letter or a digit
 *    If so, it sets the valid variable to true
 * 3. To many 'if's
 */

bool isUniqueName(const std::string& name)
{
	// in a real word scenario we would have checked against a list of names return true;
}
bool isNotValid(char ch)
{
	return !isalpha(ch) && !isdigit(ch) && ch != '$' && ch != '.' && ch != '!';
}
bool isValidUserName(const std::string& username)
{
	/*
	bool valid = false;
	if (username.length() >= MIN_NAME_LENGTH)
	{
		if (username.length() <= MAX_NAME_LENGTH)
		{
			if (isalpha(username[0]))
			{
				if (isdigit(username[1]))
				{
					bool foundNotValidChar = std::find_if(username.begin(), username.end(), isNotValid) != username.end();
					if (!foundNotValidChar)
					{
						valid = isUniqueName(username);
					}
				}
			}
		
	}
	return valid;*/

	return username.length() >= MIN_NAME_LENGTH && 
		   username.length() <= MAX_NAME_LENGTH &&
		   isalpha(username[0]) &&
		   isdigit(username[1]) &&
		   std::find_if(username.begin(), username.end(), isNotValid) == username.end();
}