#include <iostream>

std::string getDay(int num);

int main()
{
	for (int i = 1; i < 9; i++)
	{
		std::cout << i << ". " << getDay(i).c_str() << std::endl;
	}

	system("pause");
	return 0;
}

// The function is simple because it only contains a single switch-case
std::string getDay(int num)
{
	switch(num)
	{
	case 1:
		return "Sunday";
	case 2:
		return "Monday";
	case 3:
		return "Tuesday";
	case 4:
		return "Wednesday";
	case 5:
		return "Thursday";
	case 6:
		return "Friday";
	case 7:
		return "Saturday";
	}

	return "invalid day";
}
