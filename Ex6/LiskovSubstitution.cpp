#include <iostream>

/**
 * 1. No, Drum can't run the playChord function.
 */

class Instrument
{
public:
	virtual void play() const = 0;
};
class Guitar : public Instrument
{
public:
	void play() const override
	{
		std::cout << "Am" << std::endl;
	}
};
class Drum : public Instrument
{
	void play() const override
	{
		std::cout << "*Drum Sound*" << std::endl;
	}
};
void Musician(const Instrument& instrument)
{
	instrument.play();
}