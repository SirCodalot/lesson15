#include <iostream>

struct IPhone
{
	virtual void call() const = 0;
};

struct ISMSable
{
	virtual void sms() const = 0;
};

struct IFaxable
{
	virtual void fax() const = 0;
};

struct HomePhone : public IPhone, IFaxable
{
	void call() const override
	{
		std::cout << "E.T. Phone Home" << std::endl;
	}
	void fax() const override
	{
		std::cout << "E.T. Fax Home" << std::endl;
	}
};

struct SmartPhone : public IPhone, ISMSable
{
	void call() const override
	{
		std::cout << "*calling*" << std::endl;
	}

	void sms() const override
	{
		std::cout << "*sms-ing*" << std::endl;
	}

	/*
	void fax() const override
	{
		throw std::runtime_error("unsupported function");
	}
	*/
};