#include <cmath>

#define PI 3.14159265359

/**
 * 1. In order to add another shape, a new case needs to be added to the calcArea function.
 * 
 * 2. A new "getEdge" function should be created for the rectangle's area, and a Point struct should be made for the points (According to DRY)
 * 
 */

struct Circle
{
	int x;
	int y;
	int radius;
};
struct Rectangle
{
	int x1;
	int y1;
	int x2;
	int y2;
};
double calcArea(void *shape, int type)
{
	double tmp = 0;
	switch (type)
	{
	case 1:
	{
		Circle *pCircle = (Circle*)shape;
		tmp = PI * std::pow(pCircle->radius, 2);
		break;
	}
	case 2:
	{
		Rectangle *pRectangle = (Rectangle*)shape;
		tmp = (pRectangle->x2 - pRectangle->x1) * (pRectangle->y2 - pRectangle->y1);
		break;
	}
	}
	return tmp;
}