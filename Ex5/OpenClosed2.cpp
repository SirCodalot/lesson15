#include <cmath>

#define PI 3.14159265359

struct Point
{
	int x, y;
};

struct Shape
{
	virtual double calcArea() const = 0;
};

struct Circle : public Shape
{
	Point center;
	int radius;

	double calcArea() const override
	{
		return PI * std::pow(radius, 2);
	}
};
struct Rectangle : public Shape
{
	Point a, b;

	double calcArea() const override
	{
		return (b.x - a.x) * (b.y - a.y);
	}
};
double calcArea(Shape *shape)
{
	return shape->calcArea();
}